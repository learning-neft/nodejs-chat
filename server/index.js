const 
    wss = require("ws").Server,
    server = new wss({
        host: '192.168.4.56',
        port: 8080
    });

let
    clients = new Set(),
    messages = [];

server.on("listening", function(){
    console.log('Скрипт успешно запущен');
});

server.on("connection", function(socket){
    console.log('Подключился новый клиент');
    clients.add(socket);
    socket.on("message", function(message){
        console.log('Пришло новое сообщение: ' + message);
        const json = JSON.parse(message);
        switch(json.type){
            case 'output':
                messages.push({
                    message: json.data.message
                });
                for (let client of clients){
                    if (client != socket){
                        client.send(JSON.stringify({
                            type: 'input',
                            data: {
                                from: {
                                    nick: 'Я',
                                    photo: ''
                                },
                                message: json.data.message,
                                time: new Date()
                            }
                        }))
                    }
                }
                break;
        }
    });
    socket.on("close", function(socket){
        clients.delete(socket);
    });
});